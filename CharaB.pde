public class CharaB extends Character {

  CharaB(PApplet papp) {

    p = papp;

    Ani.init(p);

    moveDuration = 130;
    tippingHeight = 10;

    shapeHeight = 200;
    shapeWidth = 100;
    shapeDepth = 100;
    rotation = 0;
    centreX = 530;
    centreY = height-shapeHeight/2;
    centreZ = -900;
    this.reset();
  }

  int Yrecord;
  public void draw() {

    this.drawCharacter();

    script.ContinueExecute();

    //if(millis()%20000>=10000)script.execute("help");

    if (hittestAll()) {
      if (seq!=null)seq.pause();
      Y = Yrecord;
      centreY = Y;
    } else {
      Yrecord = Y;
      if (centreY!=height-shapeHeight/2 && !jumping())drop(Y/2);
    }

    boolean myKeyPressed = false;
    if (currentState.ordinal() >= STATE.HELP.ordinal()) {
      if (keypress[int('w')]) {
        myKeyPressed = true;
        //this.say("jump");
        //this.jump(500, 1000);
        this.moveNorth(10);
      }
      if (keypress[int('s')]) {
        myKeyPressed = true;
        this.moveSouth(10);
      }
      if (keypress[int('a')]) {
        myKeyPressed = true;
        this.moveWest(10);
      }
      if (keypress[int('d')]) {
        myKeyPressed = true;
        this.moveEast(10);
      }
      if (keypress[SHIFT_KEY]) {
        myKeyPressed = true;
        //if(DEBUGMODE)this.say("jump");
        this.jump(150, 350);
      }
      if (!myKeyPressed && !this.jumping()) {
        this.reset();
      }
    }
    if (myKeyPressed) {
      ctr.notified("keypressed");
    }
  }
}