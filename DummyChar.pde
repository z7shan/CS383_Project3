public class DummyChar extends Character {
  
  PApplet p;
  
  Script s = new Script(this);
  
  DummyChar(PApplet plet, int x, int z) {
    p=plet;
    
    moveDuration = 130;
    tippingHeight = 10;
    
    shapeWidth = (int)random(60, 150);
    shapeDepth = (int)random(60, 150);
    shapeHeight = (int)random(70, 220);
    //centreX = (int)random(x+shapeWidth/2, x+bwidth-shapeWidth/2);
    centreX = x+bwidth/2;
    centreY = height-shapeHeight/2;
    centreZ = (int)random(z-bdepth+shapeWidth/2, z-shapeWidth/2);
    this.reset();
    
    s.execute("moveleftright");
  }
  
  void draw(){
    this.drawCharacter();
    s.ContinueExecute();
  }
}