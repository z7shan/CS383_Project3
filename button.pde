public class Button extends Shape {

  boolean on = false;

  int Num;
  
  int timer = -1;

  Button() {
    shapeWidth = 50;
    shapeDepth = 50;
    shapeHeight = 50;

    shapes.add(this);
    Num = buttons.size();
    buttons.add(this);
  }

  public void draw() {
    pushMatrix();
    fill(255);
    translate(X, Y, Z);
    box(shapeWidth, shapeDepth, shapeHeight);
    noFill();
    popMatrix();
  }

  public void press() {
    if(timer==-1){
      timer = millis();
    }
    if(!(timer!=-1 && millis() - timer >= 1000)){
       return;
    } else {
      timer = -1;
    }
    //shapeWidth = 100;
    //shapeDepth = 100;
    //shapeHeight = 100;
    switch(Num) {
    case 0:
      on = !on;
      if (on)ctr.notified("button0_on");
      else ctr.notified("button0_off");
      break;
    case 1:
      on = !on;
      if (on)ctr.notified("button1_on");
      else ctr.notified("button1_off");
      break;
    }
  }
}