import java.awt.*;

public abstract class Character extends Shape {

  PApplet p;
  AniSequence seq;

  Script script = new Script(this);

  String words = "";

  protected int moveDuration;
  protected int tippingHeight;

  protected void drawCharacter() {
    if(buttons.get(0).hittest(this)){
      buttons.get(0).press();
    }
    if(buttons.get(1).hittest(this)){
      buttons.get(1).press();
    }
    
    lights();
    pushMatrix();
    translate(X, Y, Z);
    pushMatrix();
    //rotateY(radians(0));
    rotateY(radians(rotation));
    if (strongerlight_switch) {
      fill(255);
      stroke(0);
      strokeWeight(2);
      box(shapeWidth, shapeHeight, shapeDepth);
    } else {
      fill(0);
      stroke(255);
      strokeWeight(2);
      box(shapeWidth, shapeHeight, shapeDepth);
    }
    popMatrix();

    this.drawWords();
    popMatrix();
  }

  protected void drawWords() {
    int gap = 20;
    float h = 50;
    float aboveShape = 80;

    float w = textWidth(words);
    if (words.length()!=0) {
      //noFill();
      fill(0);
      stroke(255);
      strokeWeight(2);
      rect(-gap, -shapeHeight/2-aboveShape-h-gap/2, w+gap*2, h+gap*2);
    }
    fill(255);
    textSize(h);
    textMode(SHAPE);
    text(words, 0, -shapeHeight/2 - aboveShape);
  }

  public void turnWest() {
    if (rotation == 180) rotation = -180;
    Ani.setDefaultEasing(Ani.LINEAR);
    Ani.to(this, 0.1, "rotation", -90);
  }

  public void turnEast() {
    if (rotation == -180) rotation = 180;
    Ani.setDefaultEasing(Ani.LINEAR);
    Ani.to(this, 0.1, "rotation", 90);
  }

  public void turnNorth() {
    int temp;
    if (rotation >= 0) temp = 180;
    else temp = -180;
    Ani.setDefaultEasing(Ani.LINEAR);
    Ani.to(this, 0.1, "rotation", temp);
  }

  public void turnSouth() {
    Ani.setDefaultEasing(Ani.LINEAR);
    Ani.to(this, 0.1, "rotation", 0);
  }

  protected void tipping() {
    if (!this.jumping()) {
      if (millis()%moveDuration+1 > moveDuration/2) Y = centreY - tippingHeight;
      else Y = centreY;
    }
  }

  public void moveWest(int speed) {
    this.turnWest();
    if (X-shapeWidth/2 == room().x) return; 
    if (X-shapeWidth/2 < room().x) {
      centreX = room().x + shapeWidth/2;
      this.reset();
      return;
    }
    X -= speed;
    if (hittestAll()) {
      X += speed;
      return;
    }
    centreX -= speed;
    this.tipping();
  }

  public void moveEast(int speed) {
    this.turnEast();
    if(X+shapeWidth/2 == room().x + room().width) return;
    if (X+shapeWidth/2 > room().x + room().width) {
      centreX = room().x + room().width - shapeWidth/2;
      this.reset();
      return;
    }
    X += speed;
    if (hittestAll()) {
      X -= speed;
      return;
    }
    centreX += speed;
    this.tipping();
  }

  public void moveNorth(int speed) {
    this.turnNorth();
    if (Z-shapeWidth/2 == room().y - room().height) return; 
    if (Z-shapeWidth/2 < room().y - room().height) {
      centreZ = room().y - room().height + shapeWidth/2;
      this.reset();
      return;
    }
    Z -= speed;
    if (hittestAll()) {
      Z += speed;
      return;
    }
    centreZ -= speed;
    this.tipping();
  }

  public void moveSouth(int speed) {
    this.turnSouth();
    if((Z+shapeWidth/2 == room().y)) return;
    if (Z+shapeWidth/2 > room().y) {
      centreZ = room().y - shapeWidth/2;
      this.reset();
      return;
    }
    Z += speed;
    if (hittestAll()) {
      Z -= speed;
      return;
    }
    centreZ += speed;
    this.tipping();
  }

  public void jump(int h, int time) {
    if (this.jumping()) return;
    float t = time/1000.0/2;  
    float t2 = ((height-Y+h)/float(shapeHeight/2+h))*t;
    seq = new AniSequence(p);
    seq.beginSequence();
    Ani.setDefaultEasing(Ani.SINE_OUT);
    seq.add(Ani.to(this, t, "Y", Y-h));
    Ani.setDefaultEasing(Ani.SINE_IN);
    seq.add(Ani.to(this, t2, "Y", height-shapeHeight/2));
    seq.endSequence();
    seq.start();
  }

  public void drop(int time) {
    //if(jumping()) return;
    Y += 5;
    if (hittestAll()) {
      Y -= 5;
      return;
    }
    centreY = height-shapeHeight/2;
    float t = time/1000.0;  
    seq = new AniSequence(p);
    seq.beginSequence();
    Ani.setDefaultEasing(Ani.QUAD_IN);
    seq.add(Ani.to(this, t, "Y", centreY));
    seq.endSequence();
    seq.start();
  }

  public void say(String lines) {
    this.words = lines;
  }

  protected boolean jumping() {
    return seq!=null && (!seq.isEnded() && seq.isPlaying());
  }

  public Rectangle room() {
    int x_index = (centreX + bwidth)/bwidth -1;
    int x = x_index * bwidth;
    int z_index = (centreZ - bdepth)/bdepth +1;
    int z = z_index * bdepth;
    //println(x, z, bwidth, bdepth);
    return new Rectangle(x, z, bwidth, bdepth);
  }

  public boolean insideRoom(Rectangle room) {
    boolean west = centreX-shapeWidth/2 > room.x;
    boolean east = centreX+shapeWidth/2 < room.x + room.width;
    boolean south = centreZ+shapeWidth/2 < room.y;
    boolean north = centreZ-shapeWidth/2 > room.y - room.height;
    return west && east && south && north;
  }

  public void execute(String file) {
    script.execute(file);
  }

  //@Override
  //  public boolean hittest(Shape s) {
  //  boolean result = super.hittest(s);
  //  if (result && s == button) {
  //    button.press();
  //  }
  //  return result;
  //}
  
  protected boolean hittestAll() {
    for (Shape s : shapes) {
      if (s != this && this.hittest(s)) {
        if (DEBUGMODE)text("hit", 400, 400);
        return true;
      }
    }
    return false;
  }
}