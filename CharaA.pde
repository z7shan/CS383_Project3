public class CharaA extends Character {

  CharaA(PApplet plet) {
    p = plet;

    Ani.init(p);

    moveDuration = 110;
    tippingHeight = 17;

    shapeWidth = 100;
    shapeHeight = 100;
    shapeDepth = 100;
    rotation = 0;
    centreX = 1030;
    centreY = height-shapeHeight/2;
    centreZ = -500;
    this.reset();
  }

  int Yrecord;
  public void draw() {
    this.drawCharacter();
    script.ContinueExecute();

    if (hittestAll()) {
      if (seq!=null)seq.pause();
      Y = Yrecord;
      centreY = Y;
    } else {
      Yrecord = Y;
      if (centreY!=height-shapeHeight/2 && !jumping()){
        drop(Y/2);
      }
    }

    boolean myKeyPressed = false;
    //println(currentState.ordinal(),STATE.HELP.ordinal());
    if (currentState.ordinal() >= STATE.HELP.ordinal()) {
      //case UP:
      if (keypress[UP_KEY]) {
        myKeyPressed = true;
        this.moveNorth(13);
      }
      //case DOWN:
      if (keypress[DOWN_KEY]) {
        myKeyPressed = true;
        this.moveSouth(13);
      }
      //case LEFT:
      if (keypress[LEFT_KEY]) {
        myKeyPressed = true;
        //if(DEBUGMODE)script.execute("sayleft");
        this.moveWest(13);
      }
      //case RIGHT:
      if (keypress[RIGHT_KEY]) {
        myKeyPressed = true;
        //if(DEBUGMODE)script.execute("sayright");
        this.moveEast(13);
      }
      if (keypress[int(' ')]) {
        myKeyPressed = true;
        //if(DEBUGMODE)script.execute("sayjump");
        this.jump(300, 600);
      }
      if (!myKeyPressed && !this.jumping()) {
        this.reset();
      }
    }
    if (myKeyPressed) {
      ctr.notified("keypressed");
    }
  }
}