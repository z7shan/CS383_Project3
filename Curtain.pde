public class Curtain {

  private int Y = 0;

  private PApplet p;

  private boolean closed = true;

  Curtain(PApplet p) {
    this.p = p;
    Ani.init(p);
    Ani.setDefaultEasing(Ani.LINEAR);
  }

  public void draw() {
    if (Y==0) {
      closed = true;
    }
    else closed = false;

    pushMatrix();

    translate(0, 0, -Y);

    stroke(0);
    strokeWeight(10);
    line(1, height, 0, 1, height, bdepth);
    line(bwidth-1, height, 0, bwidth-1, height, bdepth);
    popMatrix();

    pushMatrix();

    translate(0, Y, 0);

    stroke(0);
    strokeWeight(7);
    line(0, height, -bdepth, 0, 0, -bdepth);
    line(0, height, 0, 0, 0, 0);
    line(0, height, 0, 0, height, bdepth);
    line(bwidth, height, -bdepth, bwidth, 0, -bdepth);
    line(bwidth, height, 0, bwidth, 0, 0);
    line(bwidth, height, 0, bwidth, height, bdepth);

    if (Y>-height) {
      pushMatrix();
      translate(0, -Y, 0);
      line(0, 0, 0, 0, 0, bdepth);
      line(bwidth, 0, 0, bwidth, 0, bdepth);
      popMatrix();
      noFill();

      // draw the frame
      if (Y!=0) {
        stroke(255);
        strokeWeight(8);
        beginShape();
        vertex(0, height, 0);
        vertex(0, height, -bdepth);
        vertex(bwidth, height, -bdepth);
        vertex(bwidth, height, 0);
        vertex(0, height, 0);
        endShape();
      }
    }

    fill(0);
    noStroke();
    beginShape();
    vertex(-1, height, bdepth);
    vertex(-1, height, -bdepth);
    vertex(-1, 0, -bdepth);
    vertex(-1, 0, bdepth);
    vertex(-1, height, bdepth);
    endShape();

    beginShape();
    vertex(0, height, -bdepth);
    vertex(bwidth, height, -bdepth);
    vertex(bwidth, 0, -bdepth);
    vertex(0, 0, -bdepth);
    vertex(0, height, -bdepth);
    endShape();

    if (special_use_flag) {
      beginShape();
      vertex(bwidth+1, height, bdepth);
      vertex(bwidth+1, height, -bdepth);
      vertex(bwidth+1, 0, -bdepth);
      vertex(bwidth+1, 0, bdepth);
      vertex(bwidth+1, height, bdepth);
      endShape();
    }

    popMatrix();
  }

  public void rise() {
    float t = map(Y, 0, -height-100, 5, 0);
    Ani.to(this, t, "Y", -height-100);
  }

  public void drop() {
    float t = map(Y, -height-100, 0, 5, 0);
    Ani.to(this, t, "Y", 0);
  }

  public boolean isClosed() {
    return closed;
  }
}