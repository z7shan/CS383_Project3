import de.looksgood.ani.*;
import processing.video.*;
import gab.opencv.*;

boolean DEBUGMODE = false;

Capture cam;
OpenCV opencv;
PImage output;

Rectangle[] faces;

CharaA a;
CharaB b;

int players;

ArrayList<Shape> shapes = new ArrayList<Shape>();

float fov = radians(40);
int z = 1001;

boolean[] keypress;

DummyChar[][] dummies;

Curtain curtain;
Platform p1;
Platform p2;
ArrayList<Button> buttons = new ArrayList<Button>();

ArrayList<PImage> photos;

void setup() {

  randomSeed(System.currentTimeMillis());

  keypress = new boolean[128];
  photos = new ArrayList<PImage>();

  //size(640,480,P3D);
  fullScreen(P3D);

  perspective(fov, float(width)/float(height), z - 1000, z + 90000);
  beginCamera();
  camera();
  translate(0, 0, -z);
  //int r = int(map(mouseX, 0, width, -30, 30));
  //rotateY(radians(r));
  endCamera();

  a = new CharaA(this);
  b = new CharaB(this);

  players = 0;

  background(0);

  bwidth = width;
  bdepth = 2000;

  dummies = new DummyChar[3][layers];
  for (int i=0; i<3; i++) {
    for (int j=0; j<layers; j++) {
      if (i==1 && j==1) continue;
      dummies[i][j] = new DummyChar(this, -bwidth+bwidth*i, bdepth-bdepth*j);
    }
  }

  lightswitch = false;
  strongerlight_switch = false;

  currentState = STATE.SLEEP;

  curtain = new Curtain(this);
  p1 = new Platform(this, 400, 200, 30);
  p1.setInitialXYZ(-110, 600, -500);
  p1.setXYZ(1500, 600, -500);
  
  p2 = new Platform(this, 200, 200, 50);
  p2.setInitialXYZ(810, 0, -500);
  p2.setXYZ(810, 400, -500);
  
  shapes.add(a);
  shapes.add(b);
  

  Timer=-1;
}

int bwidth;
int bdepth;

int layers = 40;
boolean lightswitch;
boolean strongerlight_switch;
boolean showPhotos;

enum STATE { 
  SLEEP, HELP, READY_PLAYER_ONE, READY_PLYER_TWO;
};
STATE currentState;
Controller ctr = new Controller();
int Timer;

boolean special_use_flag;

void draw() {
  background(0);
  smooth();

  if (cam == null) {
    cam = new Capture(this, int(640)/2, int(480)/2);
    opencv = new OpenCV(this, cam.width, cam.height);
    opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE); 
    cam.start();
    output = new PImage(cam.width, cam.height);
  }
  
  int t = millis();

  if (t%1000<=100) {
    if (cam.available() == true) {
      cam.read();
      opencv.loadImage(cam);
      //opencv.flip(1);
      faces = opencv.detect();
      opencv.useColor(RGB);
      output = cam.copy();
    }
  }

  if (t%1000 <= 1000/60.0) {
    photos.add(output);
  }

  //int lightswitch_sec = 10;
  //if (millis()%(lightswitch_sec*2000)>=lightswitch_sec*1000) {
  if (!curtain.isClosed()) {
    special_use_flag = true;
    this.drawBkgd();
    for (int i=0; i<3; i++) {
      for (int j=0; j<layers; j++) {
        if (i==1 && j==1) continue;
        dummies[i][j].draw();
      }
    }
  } else {
    special_use_flag = false;
  }

  curtain.draw();
  p1.draw();
  p2.draw();

  drawFloorAndCeiling();

  ctr.events();

  a.draw();
  b.draw();

  if (DEBUGMODE) {
    pushMatrix();
    image(output, 0, 0);
    popMatrix();

    pushMatrix();
    if (faces!=null && faces.length!=0) {
      for (int i=0; i<faces.length; i++) {
        Rectangle r = faces[i];
        noFill();
        stroke(255, 0, 0);
        rect((int)r.getX(), (int)r.getY(), (int)r.getWidth(), (int)r.getHeight());
        stroke(255);
      }
    }
    popMatrix();
  }
}

void drawFloorAndCeiling() {
  if (strongerlight_switch) {
    fill(255);
    stroke(0);
  } else {
    fill(0);
    stroke(255);
  }
  strokeWeight(2);
  beginShape();
  vertex(0, height, 0);
  vertex(0, height, -bdepth);
  vertex(bwidth, height, -bdepth);
  vertex(bwidth, height, 0);
  vertex(0, height, 0);
  endShape();

  beginShape();
  vertex(0, 0, 0);
  vertex(0, 0, -bdepth);
  vertex(bwidth, 0, -bdepth);
  vertex(bwidth, 0, 0);
  vertex(0, 0, 0);
  endShape();
  noFill();
}

void drawBkgd() {
  int index = 0;
  for (int i=0; i<layers; i++) {
    noFill();
    beginShape();
    vertex(0, height, -bdepth*i);
    vertex(0, 0, -bdepth*i);
    vertex(bwidth, 0, -bdepth*i);
    vertex(bwidth, height, -bdepth*i);
    vertex(0, height, -bdepth*i);
    endShape();

    if (strongerlight_switch) {
      pushMatrix();
      translate((bwidth-output.width*1.75)/2, (height-output.height*1.75)/2, -bdepth*i-1001);
      scale(1.75);
      image(photos.get(index), 0, 0);
      popMatrix();
      index = (index+1)%photos.size();

      fill(255);
      stroke(0);
    } else {
      noFill();
    }
    beginShape();
    vertex(0, height, -bdepth*(i-1));
    vertex(0, height, -bdepth*i);
    vertex(bwidth, height, -bdepth*i);
    vertex(bwidth, height, -bdepth*(i-1));
    vertex(0, height, -bdepth*(i-1));
    endShape();

    //if(strongerlight_switch){
    //  fill(255);
    //  stroke(0);
    //} else {
    //  noFill();
    //}
    beginShape();
    vertex(0, 0, -bdepth*(i-1));
    vertex(0, 0, -bdepth*i);
    vertex(bwidth, 0, -bdepth*i);
    vertex(bwidth, 0, -bdepth*(i-1));
    vertex(0, 0, -bdepth*(i-1));
    endShape();
    noFill();

    //DummyChar dummy = new DummyChar(this, 0, -bdepth*(i-1));
    //dummy.draw();
    //////////////////////////////////////////////////
    beginShape();
    vertex(0, height, -bdepth*i);
    vertex(0, 0, -bdepth*i);
    vertex(-bwidth, 0, -bdepth*i);
    vertex(-bwidth, height, -bdepth*i);
    vertex(0, height, -bdepth*i);
    endShape();
    if (strongerlight_switch) {
      pushMatrix();
      translate((-bwidth+output.width*1.75)/2, (height-output.height*1.75)/2, -bdepth*i-1001);
      scale(1.75);
      image(photos.get(index), 0, 0);
      popMatrix();
      index = (index+1)%photos.size();
      fill(255);
      stroke(0);
    } else {
      noFill();
    }
    beginShape();
    vertex(0, height, -bdepth*(i-1));
    vertex(0, height, -bdepth*i);
    vertex(-bwidth, height, -bdepth*i);
    vertex(-bwidth, height, -bdepth*(i-1));
    vertex(0, height, -bdepth*(i-1));
    endShape();

    //if(strongerlight_switch){
    //  fill(255);
    //  stroke(0);
    //} else {
    //  noFill();
    //}
    beginShape();
    vertex(0, 0, -bdepth*(i-1));
    vertex(0, 0, -bdepth*i);
    vertex(-bwidth, 0, -bdepth*i);
    vertex(-bwidth, 0, -bdepth*(i-1));
    vertex(0, 0, -bdepth*(i-1));
    endShape();
    noFill();

    ////////////////////////////////////////////////////
    beginShape();
    vertex(bwidth, height, -bdepth*i);
    vertex(bwidth, 0, -bdepth*i);
    vertex(bwidth*2, 0, -bdepth*i);
    vertex(bwidth*2, height, -bdepth*i);
    vertex(bwidth, height, -bdepth*i);
    endShape();
    if (strongerlight_switch) {
      pushMatrix();
      translate((bwidth-output.width*1.75)/2+bwidth, (height-output.height*1.75)/2, -bdepth*i-1001);
      scale(1.75);
      image(photos.get(index), 0, 0);
      popMatrix();
      index = (index+1)%photos.size();
      fill(255);
      stroke(0);
    } else {
      noFill();
    }
    beginShape();
    vertex(bwidth, height, -bdepth*(i-1));
    vertex(bwidth, height, -bdepth*i);
    vertex(bwidth*2, height, -bdepth*i);
    vertex(bwidth*2, height, -bdepth*(i-1));
    vertex(bwidth, height, -bdepth*(i-1));
    endShape();

    //if(strongerlight_switch){
    //  fill(255);
    //  stroke(0);
    //} else {
    //  noFill();
    //}
    beginShape();
    vertex(bwidth, 0, -bdepth*(i-1));
    vertex(bwidth, 0, -bdepth*i);
    vertex(bwidth*2, 0, -bdepth*i);
    vertex(bwidth*2, 0, -bdepth*(i-1));
    vertex(bwidth, 0, -bdepth*(i-1));
    endShape();
    
  }
  stroke(255);
  strokeWeight(1);
  line(bwidth, height, -bdepth+70, bwidth*2, height, -bdepth+70);
  line(bwidth-70, height, -bdepth, bwidth-70, height, -bdepth*layers);
  line(-bwidth, height, -bdepth/2, -70, height, -bdepth/2);
  line(-70, height, -bdepth/2, -70, height, -bdepth+70);
  line(-70, height, -bdepth+70, 0, height, -bdepth+70);
  line(70, height, -bdepth, 60, height, -bdepth*2 + 70);
  line(70, height, -bdepth*2 + 70, bwidth-70, height, -bdepth*2 + 70);
  noFill();
}

int angle = 0;

/*
void mouseDragged() {
 
 z = int(map(mouseY, 0, height, 8000, 2000));
 
 perspective(fov, float(width)/float(height), z - 2000, z + 2000);
 
 beginCamera();
 camera();
 translate(0, 0, -z);
 //int r;
 //if(mouseX-pmouseX < 0){
 //  r = int(map(pmouseX - mouseX, pmouseX, pmouseX - width, 0, -2));
 //} else {
 //  r = int(map(mouseX - pmouseX, pmouseX + width, pmouseX, 0, 2));
 //}
 //angle += r;
 //rotateY(radians(angle));
 endCamera();
 }
 */

void keyPressed() {
  switch(keyCode) {
  case UP:
    keypress[UP_KEY] = true;
    return;
  case DOWN:
    keypress[DOWN_KEY] = true;
    return;
  case LEFT:
    keypress[LEFT_KEY] = true;
    return;
  case RIGHT:
    keypress[RIGHT_KEY] = true;
    return;
  case SHIFT:
    keypress[SHIFT_KEY] = true;
    return;
  }
  int KEY = (int)key;
  if (KEY>=0 && KEY<=127) {
    if (KEY>='A' && KEY<='Z') KEY += int('a')-int('A');
    keypress[KEY] = true;
  }


  if (key == 'l') {
    if (!lightswitch) {
      curtain.rise();
      a.execute("wow");
      b.execute("omg");
    } else {
      curtain.drop();
    }
    lightswitch = !lightswitch;
  }
  if (key == 'k') {
    if (!strongerlight_switch) {
      //curtain.rise();
      a.execute("wow");
      b.execute("omg");
    } else {
      //curtain.drop();
    }
    strongerlight_switch = !strongerlight_switch;
  }
}

void keyReleased() {
  switch(keyCode) {
  case UP:
    keypress[UP_KEY] = false;
    return;
  case DOWN:
    keypress[DOWN_KEY] = false;
    return;
  case LEFT:
    keypress[LEFT_KEY] = false;
    return;
  case RIGHT:
    keypress[RIGHT_KEY] = false;
    return;
  case SHIFT:
    keypress[SHIFT_KEY] = false;
    return;
  }
  int KEY = (int)key;
  if (KEY>=0 && KEY<=127) {
    if (KEY>='A' && KEY<='Z') KEY += int('a')-int('A');
    keypress[KEY] = false;
  }
}


final int UP_KEY = 17;
final int DOWN_KEY = 18;
final int LEFT_KEY = 19;
final int RIGHT_KEY = 20;
final int SHIFT_KEY = 16;

public void changeStateTo(STATE state) {
  currentState = state;
  ctr.notified("");
}