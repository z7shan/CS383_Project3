public abstract class Shape {
  
  int shapeWidth;
  int shapeDepth;
  int shapeHeight;
  int rotation;
  int X;
  int Y;
  int Z;
  int centreX;
  int centreY;
  int centreZ;
  
  Shape(){
    //shapes.add(this);
  }
  
  public abstract void draw();
  
  public boolean hittest(Shape s){
    boolean x = ((X + shapeWidth/2) >= (s.X - s.shapeWidth/2)) && ((X - shapeWidth/2) <= (s.X + s.shapeWidth/2));
    boolean y = ((Y + shapeHeight/2) >= (s.Y - s.shapeHeight/2)) && ((Y - shapeHeight/2) <= (s.Y + s.shapeHeight/2));
    boolean z = ((Z + shapeDepth/2) >= (s.Z - s.shapeDepth/2)) && ((Z - shapeDepth/2) <= (s.Z + s.shapeDepth/2));
    return x && y && z;
  }
  
  protected void reset() {
    X = centreX;
    Y = centreY;
    Z = centreZ;
  }
}