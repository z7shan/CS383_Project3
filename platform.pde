public class Platform extends Shape
{
  private PApplet p;

  private boolean alive = false;

  Button bt;

  Platform(PApplet pa, int w, int d, int thickness) {
    p = pa;
    Ani.init(p);
    Ani.setDefaultEasing(Ani.SINE_IN_OUT);

    shapeWidth = w;
    shapeDepth = d;
    shapeHeight = thickness;

    bt = new Button();
    bt.X = 0;
    bt.Y = -105;
    bt.Z = shapeDepth - 50;
    
    shapes.add(this);
  }

  public void draw() {
    if (!alive) return;
    bt.X = X;
    bt.Y = Y;
    bt.Z = Z;
    lights();
    pushMatrix();
    translate(X, Y, Z);
    //println(X, Y, Z);
    //rotateY(radians(0));
    rotateY(radians(rotation));
    if (strongerlight_switch) {
      fill(255);
      stroke(0);
      strokeWeight(2);
      box(shapeWidth, shapeHeight, shapeDepth);
    } else {
      fill(0);
      stroke(255);
      strokeWeight(2);
      box(shapeWidth, shapeHeight, shapeDepth);
    }
    popMatrix();
    
    pushMatrix();
    bt.X += 100;
    bt.Y -= 255;
    translate(0, -55, 0);
    bt.draw();
    popMatrix();
  }

  public void setInitialXYZ(int x, int y, int z) {
    X = x;
    Y = y;
    Z = z;
  }

  public void setXYZ(int x, int y, int z) {
    centreX = x;
    centreY = y;
    centreZ = z;
  }

  public void showbyX() {
    alive = true;
    float t = 3;
    Ani.to(this, t, "X", centreX);
  }
  
  public void showbyY() {
    alive = true;
    float t = 3;
    Ani.to(this, t, "Y", centreY);
  }
}