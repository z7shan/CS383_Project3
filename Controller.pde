public class Controller {

  public STATE storedState = STATE.SLEEP;
  public boolean stateChanged = true;
  public boolean playOnce = true;
  
  public boolean found_first_trigger = false;
  public boolean touch_first_trigger = false;
  public boolean found_second_trigger = false;
  public boolean touch_second_trigger = false;
    
  public void events() {
    switch(currentState) {
    case SLEEP:
      if (Timer==-1) Timer = millis();
      if (millis()-Timer >= 2*1000) {
        if (faces!=null && faces.length > 0) {
          for (int i=0; i<faces.length; i++) {
            if (faces[i].width >= 50) { 
              Timer = -1;
              currentState = STATE.HELP;
              this.notified("");
              break;
            }
          }
        }
        if (playOnce) {
          a.execute("asleep");
          b.execute("bsleep");
          playOnce = false;
        }
        stateChanged = false;
      }
      break;
    case HELP:
      if (playOnce) {
        a.execute("ahelp");
        b.execute("bhelp");
        playOnce = false;
        stateChanged = false;
      }
      if (faces.length == 0 && Timer == -1) Timer = millis();
      if (faces.length != 0) Timer = -1;
      if (Timer!=-1 && millis()-Timer >= 3*1000) {
        Timer = 100000000;
        playOnce = true;
      }
      if (playOnce) {
        a.execute("adotdotdot");
        b.execute("bdotdotdot");
        playOnce = false;
        stateChanged = true;
        return;
      }
      if (stateChanged&&!a.script.isPlaying()&&!b.script.isPlaying()) {
        Timer = -1;
        currentState = STATE.SLEEP;
        this.notified("");
      }
      break;
    case READY_PLAYER_ONE:
      if (playOnce) {
        a.execute("aReadyplayer1");
        b.execute("bReadyplayer1");
        playOnce = false;
        stateChanged = false;
      }
      if ((a.X >= bwidth-a.shapeWidth && a.Z <= -bdepth+a.shapeDepth && a.Y == height - a.shapeHeight/2)||
      (b.X >= bwidth-b.shapeWidth && b.Z <= -bdepth+b.shapeDepth && b.Y == height - b.shapeHeight/2)) {
        touch_first_trigger = true;
      } else {
        touch_first_trigger = false;
      }
      if(touch_first_trigger && !found_first_trigger){
        found_first_trigger = true;
        p1.showbyX();
        a.execute("asomethinghere");
        b.execute("blookatplatform");
      }
      if((a.X <= a.shapeWidth && a.Z <= -bdepth+a.shapeDepth && a.Y == height - a.shapeHeight/2)||
      (b.X <= b.shapeWidth && b.Z <= -bdepth+b.shapeDepth && b.Y == height - b.shapeHeight/2)){
        touch_second_trigger = true;
       //println("touch 2nd");
      } else {
        touch_second_trigger = false;
      }
      if(p1.alive &&  touch_second_trigger && !found_second_trigger){
        println("show 2nd");
        found_second_trigger = true;
        p2.showbyY();
        //TODO
        
        
      }
      break;
    case READY_PLYER_TWO:
      break;
    }
  }

  boolean WOWed = false;
  boolean discussed = false;
  public void notified(String msg) {
    switch(msg) {
    case "keypressed":
      currentState = STATE.READY_PLAYER_ONE;
      break;
    case "button0_on":
      curtain.rise();
      if (!WOWed) {
        a.execute("wow");
        b.execute("omg");
        WOWed = true;
      }
      break;
    case "button0_off":
      curtain.drop();
      break;
    case "button1_on":
      strongerlight_switch = true;
      if(!discussed){
        a.execute("adotdotdot");
        b.execute("bdotdotdot");
        discussed = true;
      }
      break;
    case "button1_off":
      strongerlight_switch = false;
      break;
    }
    if (currentState != storedState) {
      storedState = currentState;
      stateChanged = true;
      playOnce = true;
      this.events();
    }
  }
}