import java.io.*;  //<>//
import java.util.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Script {

  private int timer=-1;

  String file;
  private ArrayList<String> arr;
  private int arrIndex = 0;
  private Character obj;

  Script(Character Char) {
    obj = Char;
  }

  public void execute(String file) {
    if(obj == a){
     println(file);
    }
    this.file = file;
    try {
      arr = readFile(sketchPath()+"\\scripts\\"+file+".txt");
      arrIndex = 0;
      timer = -1;
    } 
    catch(IOException ex) {
      println("No such file: "+file);
      return;
    }
    ContinueExecute();
  }

  public void ContinueExecute() {
    if (arr==null) return;
    if (timer != -1 && millis()>=timer) { 
      arrIndex++; 
      timer=-1;
      if (arrIndex == arr.size()) {
        arrIndex = -1;
        arr = null;
        this.file = "";
        return;
      }
    }
    String s = arr.get(arrIndex);
    //println(s);
    String[] sarr = split(s, ';');
    try {
      String command = sarr[0];
      //println(command);

      int speed;

      switch(command) {
      case "say":
        String lines = sarr[1];
        if (timer==-1)timer = millis() + int(sarr[2])*1000;
        obj.say(lines);
        //println(sarr[2]);
        break;
      case "move W":    // move W;speed;duration
        speed = int(sarr[1]);
        if (timer==-1)timer = millis() + int(sarr[2])*1000;
        obj.moveWest(speed);
        break;
      case "move E":    // move E;speed;duration
        speed = int(sarr[1]);
        if (timer==-1)timer = millis() + int(sarr[2])*1000;
        obj.moveEast(speed);
        break;
      case "move S":    // move E;speed;duration
        speed = int(sarr[1]);
        if (timer==-1)timer = millis() + int(sarr[2])*1000;
        obj.moveSouth(speed);
        break;
      case "move N":    // move E;speed;duration
        speed = int(sarr[1]);
        if (timer==-1)timer = millis() + int(sarr[2])*1000;
        obj.moveNorth(speed);
        break;
      case "restart":
        this.execute(file);
        break;
      }
    } 
    catch(ArrayIndexOutOfBoundsException ex) {
    }
  }

  public boolean isPlaying() {
    return arr!=null;
  }

  private ArrayList<String> readFile(String file) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;
    ArrayList<String> arr = new ArrayList<String>();

    try {
      while (true) {
        line = reader.readLine();
        if (line==null) break;
        arr.add(line);
      }
      return arr;
    } 
    finally {
      reader.close();
    }
  }
}